//
//  DataContainer.swift
//  AirQ
//
//  Created by aeronaut on 14.01.2018.
//  Copyright © 2018 aeronaut. All rights reserved.
//

import Foundation

struct DataContainer {
    
    static var stationsList : [StationModel]?
    static var sensorsList: [SensorsListModel]?
    static var airQualityData: AirQualityModel?
   
}
